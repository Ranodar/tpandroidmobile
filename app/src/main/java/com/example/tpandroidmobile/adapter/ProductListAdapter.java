package com.example.tpandroidmobile.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tpandroidmobile.R;
import com.example.tpandroidmobile.model.Product;

public class ProductListAdapter extends ListAdapter<Product, ProductViewHolder> {

    public ProductListAdapter(@NonNull DiffUtil.ItemCallback<Product> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ProductViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = getItem(position);
        holder.bind(product.getTitle());
    }

    public static class ProductDiff extends DiffUtil.ItemCallback<Product> {

        @Override
        public boolean areItemsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
            return oldItem.getTitle().equals(newItem.getTitle());
        }
    }
}

class ProductViewHolder extends RecyclerView.ViewHolder {
    private final TextView productItemTextView;
    private ProductViewHolder(@NonNull View itemView) {
        super(itemView);
        productItemTextView = itemView.findViewById(R.id.textViewRecyler);
    }

    public void bind(String text) {
        productItemTextView.setText(text);
    }
    public static ProductViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item, parent, false);
        return  new ProductViewHolder(view);
    }
}
