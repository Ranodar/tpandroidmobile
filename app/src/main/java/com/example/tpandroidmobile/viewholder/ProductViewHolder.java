package com.example.tpandroidmobile.viewholder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tpandroidmobile.R;
import com.example.tpandroidmobile.model.Product;
import com.example.tpandroidmobile.service.ProductService;

public class ProductViewHolder extends RecyclerView.ViewHolder {

    private Fragment _fragment;
    private TextView nameTextView;

    private View view;

    private ProductService productService;
    public ProductViewHolder(@NonNull View itemView, Fragment fragment) {
        this(itemView);
        _fragment = fragment;
        productService = new ProductService();
    }

    public ProductViewHolder(@NonNull View itemView) {
        super(itemView);
        nameTextView = itemView.findViewById(R.id.product_item_name_textview);
        view = itemView;
    }



    public void display(Product product, Runnable method) {
        nameTextView.setText(product.getTitle());

    }

    public static ProductViewHolder create(ViewGroup parent, Fragment fragment) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        return new ProductViewHolder(view, fragment);
    }
}
