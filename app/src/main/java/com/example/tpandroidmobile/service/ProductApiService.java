package com.example.tpandroidmobile.service;



import com.example.tpandroidmobile.model.Product;

import java.util.List;
import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ProductApiService {
    @GET("products")
    Call<List<Product>> getProducts();

    @GET("products")
    Observable<List<Product>> getProductsObservable();

    @GET("products/{id}")
    Call<Product> getProduct(@Path("id") int id);

    @GET("distancematrix/json")
    Call<Object> getDistance(@Query("destinations") String destinations, @Query("origins") String origins);

//    @Headers("content-type: enctype/multipart-formdata")
//    @POST("products")
//    void create(@Body Product product);
//
//    @PUT("products/{id}")
//    void update(@Body Product product, @Path("id") int id);
//
//    @DELETE("products/{id}")
//    void delete(@Path("id") int id);
}
