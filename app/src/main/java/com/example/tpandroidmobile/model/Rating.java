package com.example.tpandroidmobile.model;

import androidx.annotation.NonNull;

public class Rating {
    @NonNull
    private float rate;
    @NonNull
    private int count;

    public Rating(@NonNull float rate, @NonNull int count) {
        this.rate = rate;
        this.count = count;
    }

    @NonNull
    public float getRate() {
        return rate;
    }

    public void setRate(@NonNull float rate) {
        this.rate = rate;
    }

    @NonNull
    public Integer getCount() {
        return count;
    }

    public void setCount(@NonNull int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "rate=" + rate +
                ", count=" + count +
                '}';
    }
}
