package com.example.tpandroidmobile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;

import com.example.tpandroidmobile.databinding.FragmentFormProductBinding;
import com.example.tpandroidmobile.model.Product;
import com.example.tpandroidmobile.service.ProductService;
import com.example.tpandroidmobile.util.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FormProductFragment extends Fragment {

    FragmentFormProductBinding binding;

    ProductService productService;

//    private TextView textViewTitleProduct;
//    private TextView textViewPriceProduct;
//    private TextView textViewDescriptionProduct;
//    private TextView textViewCategoryProduct;
//    private TextView textViewImageProduct;
//    private TextView textViewRatingProduct;
//
//    private Button confirmButton;
    private FormProductFragmentDirections.FormProductToListProduct action;


    public FormProductFragment() {
        productService = new ProductService();
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentFormProductBinding.inflate(inflater,container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.ajouterButtton.setOnClickListener((e) -> {
                RetrofitClient.getInstance().getApiService().postProduct(1, new Product(0, binding.nameEdittext.getText().toString(), false)).enqueue(new Callback<Product>() {
                    @Override
                    public void onResponse(Call<Product> call, Response<Product> response) {
                        Product productResponse = response.body();
                        if(!productResponse.equals(null) && productResponse.getId() > 0) {
                            NavDirections action = FormFragmentProductDirections.actionFormToList();
                            NavHostFragment.findNavController(FormProductFragment.this).navigate(action);
                        }
                        else {
                            Toast.makeText(getContext(),response.code() + " "+ response.message(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
        });
    }
}