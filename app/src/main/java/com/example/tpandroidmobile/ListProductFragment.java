package com.example.tpandroidmobile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tpandroidmobile.adapter.ProductListAdapter;
import com.example.tpandroidmobile.databinding.FragmentListProductBinding;
import com.example.tpandroidmobile.model.Product;
import com.example.tpandroidmobile.service.ProductService;
import com.example.tpandroidmobile.util.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListProductFragment extends Fragment {

    private ProductListAdapter productListAdapter;
    private ProductService productService;
    private FragmentListProductBinding binding;

    public ListProductFragment() {
        productService = new ProductService();
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentListProductBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        productListAdapter = new ProductListAdapter(new ProductListAdapter.ProductDiff(), ListProductFragment.this);
        binding.recyclerProducts.setAdapter(productListAdapter);
        binding.recyclerProducts.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerProducts.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        RetrofitClient.getInstance().getApiService().getProducts().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                List<Product> products = response.body();
                productListAdapter.submitList(products);
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {

            }
        });
        //todosAdapter.submitList(todoService.getTodos());
    }
}
